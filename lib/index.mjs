/** Fallback storage provider for environments where the Storage API isn't available */
class AltStorage {
    data = {};
    getItem(key) {
        return this.data[key] || null;
    }
    setItem(key, value) {
        this.data[key] = value;
    }
    removeItem(key) {
        delete this.data[key];
    }
    clear() {
        this.data = {};
    }
    getData() {
        return this.data;
    }
    key(index) {
        return Object.keys(this.data)[index];
    }
    get length() {
        return Object.keys(this.data).length;
    }
}
export class SimpleStorage {
    storageSource;
    constructor(storageType) {
        if (!this.isLocalAndSessionStorageSupported()) {
            this.storageSource = new AltStorage();
            return;
        }
        if (storageType === "session") {
            this.storageSource = window.sessionStorage;
            return;
        }
        if (storageType === "local") {
            this.storageSource = window.localStorage;
            return;
        }
        throw new Error(`Invalid storage type: ${storageType}`);
    }
    setItem(key, data) {
        const value = typeof data === "string" ? data : JSON.stringify(data);
        this.storageSource.setItem(key, value);
    }
    getItem(key) {
        const value = this.storageSource.getItem(key);
        if (value === null) {
            return value;
        }
        try {
            return JSON.parse(value);
        }
        catch (err) {
            return value;
        }
    }
    removeItem(key) {
        return this.storageSource.removeItem(key);
    }
    /** Remove all items from storage */
    clear() {
        return this.storageSource.clear();
    }
    get length() {
        return this.storageSource.length;
    }
    getAllItems() {
        const items = [];
        for (let i = this.length - 1; i >= 0; i--) {
            const key = this.storageSource.key(i);
            if (key !== null) {
                const value = this.getItem(key);
                items.push([key, value]);
            }
        }
        return items;
    }
    getAllItemsAsync() {
        return new Promise((resolve) => setTimeout(() => resolve(this.getAllItems())));
    }
    isLocalAndSessionStorageSupported() {
        const testKeyValue = "_simple-storage-test";
        try {
            // Disabling cookies can cause access to window.sessionStorage or window.localStorage to throw an exception
            if (typeof window === "undefined" ||
                !window.sessionStorage ||
                !window.localStorage) {
                return false;
            }
            // iOS in private mode causes exceptions when trying to write a new storage object, see
            // https://stackoverflow.com/questions/14555347/html5-localstorage-error-with-safari-quota-exceeded-err-dom-exception-22-an
            window.sessionStorage.setItem(testKeyValue, testKeyValue);
            window.sessionStorage.removeItem(testKeyValue);
        }
        catch (err) {
            return false;
        }
        return true;
    }
}
export const simpleSessionStorage = new SimpleStorage("session");
export const simpleLocalStorage = new SimpleStorage("local");
