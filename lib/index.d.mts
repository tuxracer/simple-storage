export declare type SimpleStorageType = "session" | "local";
export declare type SimpleStorageItem = [string, unknown];
export declare class SimpleStorage {
    private storageSource;
    constructor(storageType: SimpleStorageType);
    setItem(key: string, data: unknown): void;
    getItem(key: string): unknown;
    removeItem(key: string): void;
    /** Remove all items from storage */
    clear(): void;
    get length(): number;
    getAllItems(): SimpleStorageItem[];
    getAllItemsAsync(): Promise<SimpleStorageItem[]>;
    private isLocalAndSessionStorageSupported;
}
export declare const simpleSessionStorage: SimpleStorage;
export declare const simpleLocalStorage: SimpleStorage;
