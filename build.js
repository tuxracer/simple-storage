import path from "path";
import shelljs from "shelljs";
const { rm, exec, mkdir, cp } = shelljs;

const TEMP_DIRECTORY = ".temp";
const OUTPUT_DIRECTORY = "lib";

console.log("🗑️ cleaning temp directory");
rm("-rf", TEMP_DIRECTORY);

console.log("🗑️ cleaning output directory");
rm("-rf", OUTPUT_DIRECTORY);

/** build umd */
console.log("📦 building umd package");
exec("npx tsc -p tsconfig.umd.json");

/** build esm */
console.log("📦 building esm package");
exec("npx tsc -p tsconfig.esm.json");

console.log("📦 moving files to " + OUTPUT_DIRECTORY);
mkdir(OUTPUT_DIRECTORY);
[
  ["/umd/index.js", "index.js"],
  ["/umd/index.js", "index.cjs"],
  ["/esm/index.js", "index.mjs"],
  ["/esm/index.d.ts", "index.d.ts"],
  ["/esm/index.d.ts", "index.d.mts"],
].forEach(([from, to]) => {
  cp(path.join(TEMP_DIRECTORY, from), path.join(OUTPUT_DIRECTORY, to));
});

console.log("✅ build complete");
